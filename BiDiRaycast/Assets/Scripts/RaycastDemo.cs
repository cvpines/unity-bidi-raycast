using UnityEngine;

public class RaycastDemo : MonoBehaviour
{
	[SerializeField]
	private Transform _origin;

	[SerializeField]
	private Transform _endpoint;
	
	private RaycastHit[] _hitsAroundEndpoint;
	private RaycastTools.BidirectionalRaycastHit[] _hitsBetweenOriginEndpoint;
	private RaycastHit[] _hitsAroundOrigin;
	
	private void Awake()
	{
		_hitsAroundOrigin = new RaycastHit[64];
		_hitsBetweenOriginEndpoint = new RaycastTools.BidirectionalRaycastHit[64];
		_hitsAroundEndpoint = new RaycastHit[64];
	}

	private void Update()
	{
		Vector3 originPosition = _origin.position;
		Vector3 endpointPosition = _endpoint.position;
		
		// Rotate at a constant rate about the origin
		transform.Rotate (0.0f,0.0f,50.0f * Time.deltaTime);
		
		
		// Perform a raycast between the two points
		RaycastTools.BidirectionalRaycastNonAlloc(
			originPosition,
			endpointPosition,
			_hitsAroundOrigin,
			out int originResults,
			_hitsBetweenOriginEndpoint,
			out int betweenResults,
			_hitsAroundEndpoint,
			out int endpointResults);

		// Draw rays to colliders that contain the origin point
		for (int i = 0; i < originResults; i++)
		{
			Vector3 position = _hitsAroundOrigin[i].point;
			Debug.DrawLine(originPosition, position, Color.green, 0.0f, false);	
		}

		// Draw rays that passed completely through colliders
		for (int i = 0; i < betweenResults; i++)
		{
			RaycastTools.BidirectionalRaycastHit hit = _hitsBetweenOriginEndpoint[i];
			Debug.DrawLine(hit.entry.point, hit.exit.point, Color.blue, 0.0f, false);
		}

		// Draw rays to colliders that contain the endpoint
		for (int i = 0; i < endpointResults; i++)
		{
			Vector3 position = _hitsAroundEndpoint[i].point;
			Debug.DrawLine(position, endpointPosition, Color.red, 0.0f, false);
		}
	}
}
